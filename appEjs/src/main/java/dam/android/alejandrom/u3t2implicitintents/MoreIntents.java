package dam.android.alejandrom.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MoreIntents extends AppCompatActivity implements View.OnClickListener {

    private EditText etTimerText, etTimerLenght, etContactNumber, etNoteTitle, etNoteText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);

        setUI();
    }

    private void setUI() {
        Button btStartTimer, btCallContact, btOpenWifi;

        etTimerText = findViewById(R.id.etTimerText);
        etTimerLenght = findViewById(R.id.etTimerLenght);
        etContactNumber = findViewById(R.id.etContactNumber);


        btStartTimer = findViewById(R.id.btStartTimer);
        btCallContact = findViewById(R.id.btShowContact);
        btOpenWifi = findViewById(R.id.btOpenWifi);

        btStartTimer.setOnClickListener(this);
        btCallContact.setOnClickListener(this);
        btOpenWifi.setOnClickListener(this);
    }

    private void startTimer(String text, int seconds) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER)
                .putExtra(AlarmClock.EXTRA_MESSAGE, text)
                .putExtra(AlarmClock.EXTRA_LENGTH, seconds)
                //Se salta la confirmación para crear el temporizador y directamente lo crea
                .putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void callPhone(String phoneNumber) {
        //Aqui solo deja marcado el número si quisieramos que llame la ACTION seria CALL
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

//    private void createNote(String title, String text) {
//        Intent intent = new Intent(NoteIntents.ACTION_CREATE_NOTE)
//                .putExtra(NoteIntents.EXTRA_NAME, title)
//                .putExtra(NoteIntents.EXTRA_TEXT, text);
//        if (intent.resolveActivity(getPackageManager()) != null) {
//            startActivity(intent);
//        }
//    }

    private void openWifiSettings() {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }




    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btStartTimer:
                startTimer(etTimerText.getText().toString(), Integer.parseInt(etTimerLenght.getText().toString()));
                break;
            case R.id.btShowContact:
                try {
                    callPhone(etContactNumber.getText().toString());
                } catch (Exception e) {
                    etContactNumber.setError("Número no válido");
                    Log.i("Error: ", e.getMessage());
                }
                break;
            case R.id.btOpenWifi:
                openWifiSettings();
                break;
        }
    }
}