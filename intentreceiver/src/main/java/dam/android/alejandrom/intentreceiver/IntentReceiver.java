package dam.android.alejandrom.intentreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class IntentReceiver extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        Intent intent = getIntent();
        String texto = intent.getStringExtra(Intent.EXTRA_TEXT);
        TextView textView = findViewById(R.id.textView);
        textView.setText(texto);
    }
}